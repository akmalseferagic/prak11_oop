<?php
    //panggil file sebelumnya agar fungsinya bisa digunakan
    require_once 'class_lingkaran.php';
    //ambil variabel dari class sebelumnya
    echo "NILAI PHI: " . Lingkaran::PHI;
    //buat variabel baru menggunakan fungsi di class sebelumnya
    $Lingkaran1 = new Lingkaran(10);
    $Lingkaran2 = new Lingkaran(4);
    //gunakan fungsi class sebelumnya untuk mendapatkan hasil
    echo "</br> Luas lingkaran 1: " .$Lingkaran1->getLuas();
    echo "</br> Luas lingkaran 2: " .$Lingkaran2->getLuas();

    echo "</br> keliling lingkaran 1: " .$Lingkaran1->getKeliling();
    echo "</br> keliling lingkaran 2: " .$Lingkaran2->getKeliling();
?>
